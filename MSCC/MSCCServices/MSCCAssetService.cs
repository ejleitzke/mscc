﻿using Microsoft.EntityFrameworkCore;
using MSCCData;
using MSCCData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MSCCServices
{
    public class MSCCAssetService : iMSCCAsset
    {
        private MSCCContext _context;

        public MSCCAssetService(MSCCContext context)
        {
            _context = context;
        }
        public void Add(CarAsset newAsset)
        {
            _context.Add(newAsset);
            _context.SaveChanges();
        }

        public IEnumerable<CarAsset> GetAll()
        {
            return _context.CarAssets.Include(asset => asset.Event);
        }

        public CarAsset GetById(int id)
        {
            return _context.CarAssets
                .Include(asset => asset.Event)
                .FirstOrDefault(CarAsset => CarAsset.Id == id);
        }
    }
}
