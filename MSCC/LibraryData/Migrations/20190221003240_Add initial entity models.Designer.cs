﻿// <auto-generated />
using System;
using MSCCData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MSCCData.Migrations
{
    [DbContext(typeof(MSCCContext))]
    [Migration("20190221003240_Add initial entity models")]
    partial class Addinitialentitymodels
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10034")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LibraryData.Models.Driver", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address");

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<int?>("EventId");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<int?>("MembershipId");

                    b.Property<string>("TelephoneNumber");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("MembershipId");

                    b.ToTable("Drivers");
                });

            modelBuilder.Entity("MSCCData.Models.CarAsset", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Class")
                        .IsRequired();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<int?>("EventId");

                    b.Property<string>("ImageUrl")
                        .IsRequired();

                    b.Property<string>("Number")
                        .IsRequired();

                    b.Property<string>("NumberOfRaces");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.ToTable("CarAssets");

                    b.HasDiscriminator<string>("Discriminator").HasValue("CarAsset");
                });

            modelBuilder.Entity("MSCCData.Models.Class", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Classification");

                    b.HasKey("Id");

                    b.ToTable("Classes");
                });

            modelBuilder.Entity("MSCCData.Models.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("ImageUrl");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("MSCCData.Models.Heat", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Order");

                    b.Property<int>("Run");

                    b.HasKey("Id");

                    b.ToTable("Heats");
                });

            modelBuilder.Entity("MSCCData.Models.InstructorRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CarAssetId");

                    b.Property<int?>("MembershipId");

                    b.Property<DateTime>("Requested");

                    b.HasKey("Id");

                    b.HasIndex("CarAssetId");

                    b.HasIndex("MembershipId");

                    b.ToTable("InstructorRequests");
                });

            modelBuilder.Entity("MSCCData.Models.Job", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Description");

                    b.Property<int>("Title");

                    b.HasKey("Id");

                    b.ToTable("Jobs");
                });

            modelBuilder.Entity("MSCCData.Models.Meeting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("DayOfWeek");

                    b.Property<int?>("EventId");

                    b.Property<int>("Location");

                    b.Property<int>("Time");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.ToTable("Meetings");
                });

            modelBuilder.Entity("MSCCData.Models.Membership", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Created");

                    b.HasKey("Id");

                    b.ToTable("Memberships");
                });

            modelBuilder.Entity("MSCCData.Models.RegHistory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CarAssetId");

                    b.Property<int>("MembershipId");

                    b.Property<DateTime>("Registered");

                    b.HasKey("Id");

                    b.HasIndex("CarAssetId");

                    b.HasIndex("MembershipId");

                    b.ToTable("RegHistories");
                });

            modelBuilder.Entity("MSCCData.Models.Register", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CarAssetId");

                    b.Property<int?>("MembershipId");

                    b.Property<DateTime>("Registered");

                    b.HasKey("Id");

                    b.HasIndex("CarAssetId");

                    b.HasIndex("MembershipId");

                    b.ToTable("Registrations");
                });

            modelBuilder.Entity("MSCCData.Models.Season", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Year");

                    b.HasKey("Id");

                    b.ToTable("Seasons");
                });

            modelBuilder.Entity("MSCCData.Models.Status", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Statuses");
                });

            modelBuilder.Entity("MSCCData.Models.Car", b =>
                {
                    b.HasBaseType("MSCCData.Models.CarAsset");

                    b.Property<string>("Make")
                        .IsRequired();

                    b.Property<string>("Model")
                        .IsRequired();

                    b.Property<string>("Year")
                        .IsRequired();

                    b.HasDiscriminator().HasValue("Car");
                });

            modelBuilder.Entity("MSCCData.Models.Kart", b =>
                {
                    b.HasBaseType("MSCCData.Models.CarAsset");

                    b.Property<string>("Displacement")
                        .IsRequired();

                    b.Property<string>("Make")
                        .IsRequired()
                        .HasColumnName("Kart_Make");

                    b.Property<string>("Model")
                        .IsRequired()
                        .HasColumnName("Kart_Model");

                    b.HasDiscriminator().HasValue("Kart");
                });

            modelBuilder.Entity("LibraryData.Models.Driver", b =>
                {
                    b.HasOne("MSCCData.Models.Event", "Event")
                        .WithMany("Drivers")
                        .HasForeignKey("EventId");

                    b.HasOne("MSCCData.Models.Membership", "Membership")
                        .WithMany()
                        .HasForeignKey("MembershipId");
                });

            modelBuilder.Entity("MSCCData.Models.CarAsset", b =>
                {
                    b.HasOne("MSCCData.Models.Event", "Event")
                        .WithMany("CarAssets")
                        .HasForeignKey("EventId");
                });

            modelBuilder.Entity("MSCCData.Models.InstructorRequest", b =>
                {
                    b.HasOne("MSCCData.Models.CarAsset", "CarAsset")
                        .WithMany()
                        .HasForeignKey("CarAssetId");

                    b.HasOne("MSCCData.Models.Membership", "Membership")
                        .WithMany()
                        .HasForeignKey("MembershipId");
                });

            modelBuilder.Entity("MSCCData.Models.Meeting", b =>
                {
                    b.HasOne("MSCCData.Models.Event", "Event")
                        .WithMany()
                        .HasForeignKey("EventId");
                });

            modelBuilder.Entity("MSCCData.Models.RegHistory", b =>
                {
                    b.HasOne("MSCCData.Models.CarAsset", "CarAsset")
                        .WithMany()
                        .HasForeignKey("CarAssetId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MSCCData.Models.Membership", "Membership")
                        .WithMany()
                        .HasForeignKey("MembershipId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MSCCData.Models.Register", b =>
                {
                    b.HasOne("MSCCData.Models.CarAsset", "CarAsset")
                        .WithMany()
                        .HasForeignKey("CarAssetId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MSCCData.Models.Membership", "Membership")
                        .WithMany("Registrations")
                        .HasForeignKey("MembershipId");
                });
#pragma warning restore 612, 618
        }
    }
}
