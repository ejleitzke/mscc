﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Meeting
    {
        public int Id { get; set; }
        public Event Event { get; set; }

        [Required]
        public int DayOfWeek { get; set; }

        [Required]
        public int Location { get; set; }

        [Required]
        public int Time { get; set; }
    }

}
