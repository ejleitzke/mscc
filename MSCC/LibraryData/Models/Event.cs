﻿using LibraryData.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Event
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public virtual IEnumerable<Driver> Drivers { get; set; }
        public virtual IEnumerable<CarAsset> CarAssets { get; set; }

        public string ImageUrl { get; set; }
    }
}
