﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Heat
    {
        public int Id { get; set; }

        [Required]
        public int Order { get; set; }

        [Required]
        public int Run { get; set; }
    }
}
