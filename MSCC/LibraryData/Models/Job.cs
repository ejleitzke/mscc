﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Job
    {
        public int Id { get; set; }

        [Required]
        public int Title { get; set; }

        [Required]
        public int Description { get; set; }
    }
}
