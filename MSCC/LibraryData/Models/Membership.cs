﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Membership
    {
        public int Id { get; set; }

        //public decimal Dues { get; set; }

        public DateTime Created { get; set; }

        public virtual IEnumerable<Register> Registrations { get; set; }
    }
}
