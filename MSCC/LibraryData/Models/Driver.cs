﻿using MSCCData.Models;
using System;

namespace LibraryData.Models
{
    public class Driver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string TelephoneNumber { get; set; }

        public virtual Membership Membership { get; set; }
        public virtual Event Event { get; set; } 
    }

}

