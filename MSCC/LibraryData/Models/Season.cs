﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Season
    {
        public int Id { get; set; }

        [Required]
        public int Year { get; set; }
    }
}
