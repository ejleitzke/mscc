﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Class : CarAsset
    {
        [Required]
        public int Classification { get; set; }
    }
}
