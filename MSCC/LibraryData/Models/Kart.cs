﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Kart : CarAsset
    {
        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        public string Displacement { get; set; }
    }
}
