﻿using MSCCData.Models;
using System.Collections.Generic;
using System.Text;

namespace MSCCData
{
    public interface iMSCCAsset
    {
        IEnumerable<CarAsset> GetAll();
        CarAsset GetById(int id);

        void Add(CarAsset newAsset);
    }
}
