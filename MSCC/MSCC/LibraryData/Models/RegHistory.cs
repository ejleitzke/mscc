﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class RegHistory
    {
        public int Id { get; set; }

        [Required]
        public CarAsset CarAsset { get; set; }

        [Required]
        public Membership Membership { get; set; }

        [Required]
        public DateTime Registered { get; set; }
    }
}
