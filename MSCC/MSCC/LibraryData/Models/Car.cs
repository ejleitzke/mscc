﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public class Car : CarAsset
    {
        [Required]
        public string Year { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

    }
}
