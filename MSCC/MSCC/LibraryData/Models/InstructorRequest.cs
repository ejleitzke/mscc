﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MSCCData.Models
{
    class InstructorRequest
    {
        public int Id { get; set; }
        public virtual CarAsset CarAsset { get; set; }
        public virtual Membership Membership { get; set; }
        public DateTime Requested { get; set; }
    }
}
