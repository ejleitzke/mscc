﻿using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    class Status
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
