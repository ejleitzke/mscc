﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    public abstract class CarAsset
    {
        public int Id { get; set; }

        [Required]
        public string Class { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        public string NumberOfRaces { get; set; }

        public virtual Event Location { get; set; }
    }
}
