﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MSCCData.Models
{
    class Register
    {
        public int Id { get; set; }

        [Required]
        public CarAsset CarAsset { get; set; }
        public Membership Membership { get; set; }
        public DateTime Registered { get; set; }
    }
}
