﻿using Microsoft.EntityFrameworkCore;
using LibraryData.Models;
using MSCCData.Models;

namespace MSCCData
{
    public class MSCCContext : DbContext
    {
        public  MSCCContext(DbContextOptions options) : base(options) { }

        public DbSet<Car> Cars { get; set; }
        public DbSet<CarAsset> CarAssets { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Heat> Heats { get; set; }
        public DbSet<InstructorRequest> InstructorRequests { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Kart> Karts { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<RegHistory> RegHistories { get; set; }
        public DbSet<Register> Registrations { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Status> Statuses { get; set; }

    }
}
